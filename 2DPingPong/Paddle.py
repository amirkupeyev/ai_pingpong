import pygame
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
PINK = (255, 192, 203)

PADDLE_WIDTH = 15
PADDLE_HEIGHT = 100  

WINNING_SCORE = 10

class Paddle:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.width = PADDLE_WIDTH
        self.height = PADDLE_HEIGHT
        self.color = BLACK
        self.move_dist = 7

    def move(self, up=True):
        if up:
            self.y -= self.move_dist
        else:
            self.y += self.move_dist 
        
        self.color = PINK

    def draw(self, screen):
        pygame.draw.rect(screen, self.color, (self.x, self.y, self.width, self.height))

    def bounds_check(self, screen_height):
        if self.y < 0:
            self.y = 0
        elif self.y + self.height > screen_height:
            self.y = screen_height - self.height 