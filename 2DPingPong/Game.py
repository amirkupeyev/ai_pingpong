import pygame
from .Paddle import Paddle 
from .Ball import Ball

pygame.init() 

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
PINK = (255, 192, 203)

PADDLE_WIDTH = 15
PADDLE_HEIGHT = 100  

SCORE_FONT = pygame.font.SysFont("comicsans", 50)
WINNING_SCORE = 10

class GameInformation:
    def __init__(self, left_hits, right_hits, left_score, right_score):
        self.left_hits = left_hits
        self.right_hits = right_hits
        self.left_score = left_score
        self.right_score = right_score


class Game: 
    def __init__(self,screen , screen_width, screen_height):
        pygame.init()
        self.SCREEN_WIDTH = screen_width
        self.SCREEN_HEIGHT = screen_height
        self.screen = screen

        self.player1 = Paddle(50, self.SCREEN_HEIGHT // 2 - PADDLE_HEIGHT // 2)
        self.player2 = Paddle(self.SCREEN_WIDTH - 65, self.SCREEN_HEIGHT // 2 - PADDLE_HEIGHT // 2)
        self.ball = Ball(self.SCREEN_WIDTH // 2, self.SCREEN_HEIGHT // 2 , 6)
        self.left_score = 0
        self.right_score = 0
        self.left_hits = 0
        self.right_hits = 0

    def run_game(self): 
        self.ball.move() 
        self.check_collisions()
        self.update_game_state()
        self.draw_elements()
        game_info = GameInformation(
            self.left_hits, self.right_hits, self.left_score, self.right_score)
        return game_info
        
        

        

    def handle_events(self ):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    return False 
        return True
  

    def paddle_moved(self, left=True, up=True):
        if left:
            if up and self.player1.y - self.player1.move_dist < 0:
                return False
            if not up and self.player1.y + self.player1.height > self.SCREEN_HEIGHT:
                return False
            self.player1.move(up)
        else:
            if up and self.player2.y - self.player1.move_dist < 0:
                return False
            if not up and self.player2.y + self.player1.height > self.SCREEN_HEIGHT:
                return False
            self.player2.move(up)

        return True

    '''def paddle_moved(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_w]:
            self.player1.move(up=True)
        if keys[pygame.K_s]:
            self.player1.move(up=False)
        if keys[pygame.K_UP]:
            self.player2.move(up=True)
        if keys[pygame.K_DOWN]:
            self.player2.move(up=False)'''

    def update_game_state(self):
        self.player1.bounds_check(self.SCREEN_HEIGHT)
        self.player2.bounds_check(self.SCREEN_HEIGHT)
        self.check_collisions()
        self.check_collision_with_paddle()
        self.check_ball_out_of_bounds()

    def check_collisions(self):
        if self.ball.y + self.ball.radius >= self.SCREEN_HEIGHT or self.ball.y - self.ball.radius <= 0:
            self.ball.y_dist *= -1

    def draw_elements(self):
        self.screen.fill(WHITE) 
        left_score_text = SCORE_FONT.render(f"{self.left_hits}", 1, BLACK)
        right_score_text = SCORE_FONT.render(f"{self.right_hits}", 1, BLACK)
        self.screen.blit(left_score_text, (self.SCREEN_WIDTH // 4 - left_score_text.get_width() // 2, 20))
        self.screen.blit(right_score_text, (self.SCREEN_WIDTH * (3 / 4) - right_score_text.get_width() // 2, 20))

        self.player1.draw(self.screen)
        self.player2.draw(self.screen)
        self.ball.draw(self.screen)

    def check_collision_with_paddle(self):
        if self.ball.y + self.ball.radius >= self.SCREEN_HEIGHT or self.ball.y - self.ball.radius <= 0:
            self.ball.y_dist *= -1

        if self.ball.x_dist < 0:
            if self.ball.y >= self.player1.y and self.ball.y <= self.player1.y + self.player1.height:
                if self.ball.x - self.ball.radius <= self.player1.x + self.player1.width:
                    self.ball.x_dist *= -1
                    middle_y = self.player1.y + self.player1.height / 2
                    difference_in_y = self.ball.y - middle_y
                    y_vel = difference_in_y / 8
                    self.ball.y_dist = y_vel
                    self.left_hits += 1

        else:
             if self.ball.y >= self.player2.y and self.ball.y <= self.player2.y + self.player2.height:
                if self.ball.x + self.ball.radius >= self.player2.x:
                    self.ball.x_dist *= -1
                    middle_y = self.player2.y + self.player2.height / 2
                    difference_in_y = self.ball.y - middle_y
                    y_vel = difference_in_y / 8
                    self.ball.y_dist = y_vel
                    self.right_hits += 1





    def check_ball_out_of_bounds(self): 
        if self.ball.x < 0:
                self.right_score += 1
                self.ball.reset()
        elif self.ball.x > self.SCREEN_WIDTH:
                self.left_score += 1
                self.ball.reset()

       


if __name__ == '__main__':
    width, height = 700, 500
    win = pygame.display.set_mode((width, height))
    run = Game(win , width , height)
    isrun = True
    while isrun: 
            pygame.time.Clock().tick(60)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    isrun = False
            run.run_game()
            pygame.display.update()