import pygame
import math
import random 

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
PINK = (255, 192, 203)

PADDLE_WIDTH = 15
PADDLE_HEIGHT = 100  

WINNING_SCORE = 10

class Ball:
    init_dist = 5

    def __init__(self, x, y, radius):
        self.x = self.original_x = x
        self.y = self.original_y = y
        self.radius = radius
        self.x_dist = self.init_dist
        self.y_dist = [random.choice([-0.4, 0.4 ,-0.8 ,0.6]) for _ in range(1)][0]
        self.color = PINK

    def move(self):
        self.x += self.x_dist
        self.y += self.y_dist

    def draw(self, screen):
        pygame.draw.circle(screen, self.color, (self.x, self.y), self.radius) 

    def _get_random_angle(self, min_angle, max_angle, excluded):
        angle = 0
        while angle in excluded:
            angle = math.radians(random.randrange(min_angle, max_angle))

        return angle

    def reset(self):
        self.x = self.original_x
        self.y = self.original_y
        angle = self._get_random_angle(-30, 30, [0])
        y_dist = math.sin(angle) * self.init_dist
        self.y_dist = y_dist
        self.x_dist *= -1  